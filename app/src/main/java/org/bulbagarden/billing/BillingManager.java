package org.bulbagarden.billing;

import java.util.HashMap;
import java.util.Map;

public class BillingManager {
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAz2L8a8kav" +
            "II+ee90oZpvMFEbc5VTxP1dkRAmVZq6d3o3BmknrOA054wC+MfhlKGDWfXdYBrgC2zrNH5cTSZnfo8OjiHg7U" +
            "7VTzgm0nmcIUd1H3nKR6EAECYq/pt2CTzwz+OY//DJrC5mYbqQ6vNOdrCdAPxP1jfF0AjgL3q8maxgiu9zAen" +
            "o1+aSe54SgBiXri/e6hGEMyMa1/SPfhHcIW5Ld9+sI5yrpPzq5u2ewukoKJ5TeLcltefquvSTvR17heBo646j" +
            "GxG2e2zH+29XxZ7028Ps0E1TlMR8v4Lbe6UUurtnqLc90zBojpoefMlyTr9+mOd7QryfBE6dSHHndwIDAQAB";

    public static final String SKU_LIFETIME = "lifetime_sub";
    public static final String SKU_1_MONTH = "1_month_sub";
    public static final String SKU_6_MONTH = "6_month_sub";
    public static final String SKU_12_MONTH = "12_month_sub";

    public Map<String, SkuDetails> subscriptions;

    private boolean isPro = false;

    public boolean isPro() {
        return isPro;
    }

    public void setPro(boolean pro) {
        isPro = pro;
    }

    public Map<String, SkuDetails> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Map<String, SkuDetails> subscriptions) {
        Map<String, SkuDetails> map = new HashMap<>();
        map.putAll(subscriptions);
        this.subscriptions = map;
    }

    private static final BillingManager instance = new BillingManager();

    public static BillingManager getInstance() {
        return instance;
    }

    private BillingManager() {
    }
}
