package org.bulbagarden.page;

import org.mediawiki.api.json.Api;
import org.mediawiki.api.json.ApiResult;
import org.bulbagarden.Constants;
import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.search.FullSearchArticlesTask;
import org.bulbagarden.search.SearchResults;

/**
 * Task for getting suggestions for further reading.
 * Currently powered by full-text search based on the given page title.
 */
public class SuggestionsTask extends FullSearchArticlesTask {
    private final String title;
    private final boolean requireThumbnail;

    public SuggestionsTask(Api api, WikiSite wiki, String title, boolean requireThumbnail) {
        super(api, wiki, title, Constants.MAX_SUGGESTION_RESULTS * 2, null, true);
        // request double the results wanted since we may filter some out. todo: change the api
        // request to do some filtering
        this.title = title;
        this.requireThumbnail = requireThumbnail;
    }

    @Override
    public SearchResults processResult(final ApiResult result) throws Throwable {
        return SearchResults.filter(super.processResult(result), title, requireThumbnail);
    }
}
