package org.bulbagarden.page.tabs;

import android.support.annotation.NonNull;

import org.bulbagarden.model.BaseModel;
import org.bulbagarden.page.PageBackStackItem;

import java.util.ArrayList;
import java.util.List;

public class Tab extends BaseModel {
    @NonNull private final List<PageBackStackItem> backStack = new ArrayList<>();

    @NonNull
    public List<PageBackStackItem> getBackStack() {
        return backStack;
    }
}
