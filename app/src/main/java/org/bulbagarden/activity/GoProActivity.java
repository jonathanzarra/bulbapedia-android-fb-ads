package org.bulbagarden.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.bulbagarden.R;
import org.bulbagarden.billing.BillingManager;
import org.bulbagarden.billing.IabHelper;
import org.bulbagarden.billing.IabResult;
import org.bulbagarden.billing.Purchase;
import org.bulbagarden.billing.SkuDetails;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoProActivity extends AppCompatActivity implements IabHelper.OnIabPurchaseFinishedListener {

    @BindView(R.id.price1)
    Button price1;
    @BindView(R.id.price3)
    Button price3;
    private IabHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_pro);
        ButterKnife.bind(this);

        Map<String, SkuDetails> subscriptions = BillingManager.getInstance().getSubscriptions();
        SkuDetails year = subscriptions.get(BillingManager.SKU_12_MONTH);
        if (year != null) price1.setText(year.getPrice() + " / year");
        else price1.setText("$4,99 / year");
        SkuDetails life = subscriptions.get(BillingManager.SKU_LIFETIME);
        if (life != null) price3.setText(life.getPrice() + " one time");
        else price3.setText("$6,99 one time");

    }

    @OnClick(R.id.close_button)
    public void clickClose(View v) {
        finish();
    }

    @OnClick({R.id.price1, R.id.price3})
    public void clickEveryYear(View v) {
        final String sku;
        switch (v.getId()) {
            case R.id.price1:
                sku = BillingManager.SKU_12_MONTH;
                break;
            case R.id.price3:
                sku = BillingManager.SKU_LIFETIME;
                break;
            default:
                sku = BillingManager.SKU_12_MONTH;
        }

        helper = new IabHelper(this, BillingManager.PUBLIC_KEY);
        helper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                try {
                    helper.launchPurchaseFlow(GoProActivity.this, sku, 0, GoProActivity.this);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (result.isSuccess()) {
            BillingManager.getInstance().setPro(true);
            setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!helper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
