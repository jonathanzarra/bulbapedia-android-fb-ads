package org.bulbagarden.feed.random;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import org.bulbagarden.R;
import org.bulbagarden.WikipediaApp;
import org.bulbagarden.concurrency.CallbackTask;
import org.bulbagarden.dataclient.restbase.page.RbPageSummary;
import org.bulbagarden.feed.view.FeedAdapter;
import org.bulbagarden.feed.view.StaticCardView;
import org.bulbagarden.history.HistoryEntry;
import org.bulbagarden.page.PageTitle;
import org.bulbagarden.random.RandomArticleIdTask;
import org.bulbagarden.random.RandomSummaryClient;
import org.bulbagarden.readinglist.page.database.ReadingListPageDao;
import org.bulbagarden.util.log.L;

import retrofit2.Call;

public class RandomCardView extends StaticCardView<RandomCard> {
    public RandomCardView(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setCard(@NonNull RandomCard card) {
        super.setCard(card);
        setTitle(getString(R.string.view_random_card_title));
        setSubtitle(getString(R.string.view_random_card_subtitle));
        setIcon(R.drawable.icon_feed_random);
    }

    @Override
    public void setCallback(@Nullable FeedAdapter.Callback callback) {
        super.setCallback(callback);
        setOnClickListener(new CallbackAdapter());
    }

    private class CallbackAdapter implements OnClickListener {
        @Override
        public void onClick(View view) {
            if (getCallback() != null && getCard() != null) {
                setProgress(true);
                WikipediaApp app = WikipediaApp.getInstance();
                new RandomArticleIdTask(app.getAPIForSite(app.getWikiSite()), getCard().wikiSite()) {
                    @Override
                    public void onBeforeExecute() {
                        super.onBeforeExecute();
                    }

                    @Override
                    public void onFinish(PageTitle result) {
                        super.onFinish(result);
                        setProgress(false);
                        getCallback().onSelectPage(getCard(),
                                new HistoryEntry(result, HistoryEntry.SOURCE_FEED_RANDOM));
                    }

                    @Override
                    public void execute() {
                        super.execute();
                    }

                    @Override
                    public void cancel() {
                        super.cancel();
                        setProgress(false);

                    }
                }.execute();
            }
        }

        private RandomSummaryClient.Callback serviceCallback = new RandomSummaryClient.Callback() {
            @Override
            public void onSuccess(@NonNull Call<RbPageSummary> call, @NonNull PageTitle title) {
                setProgress(false);
                if (getCallback() != null && getCard() != null) {
                    getCallback().onSelectPage(getCard(),
                            new HistoryEntry(title, HistoryEntry.SOURCE_FEED_RANDOM));
                }
            }

            @Override
            public void onError(@NonNull Call<RbPageSummary> call, @NonNull Throwable t) {
                L.w("Failed to get random card from network. Falling back to reading lists.", t);
                getRandomReadingListPage(t);
                setProgress(false);
            }
        };

        private void getRandomReadingListPage(@NonNull final Throwable throwableIfEmpty) {
            ReadingListPageDao.instance().randomPage(new CallbackTask.Callback<PageTitle>() {
                @Override
                public void success(@Nullable PageTitle title) {
                    if (getCallback() != null && getCard() != null) {
                        if (title != null) {
                            getCallback().onSelectPage(getCard(),
                                    new HistoryEntry(title, HistoryEntry.SOURCE_FEED_RANDOM));
                        } else {
                            getCallback().onError(throwableIfEmpty);
                        }
                    }
                }
            });
        }
    }
}
