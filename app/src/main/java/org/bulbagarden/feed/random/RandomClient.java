package org.bulbagarden.feed.random;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.feed.dataclient.DummyClient;
import org.bulbagarden.feed.model.Card;

public class RandomClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new RandomCard(wiki);
    }
}
