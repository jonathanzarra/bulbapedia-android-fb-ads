package org.bulbagarden.feed.news;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import org.bulbagarden.R;
import org.bulbagarden.feed.view.CardHeaderView;
import org.bulbagarden.feed.view.FeedAdapter;
import org.bulbagarden.feed.view.HorizontalScrollingListCardItemView;
import org.bulbagarden.feed.view.HorizontalScrollingListCardView;
import org.bulbagarden.util.DateUtil;
import org.bulbagarden.views.DefaultViewHolder;
import org.bulbagarden.views.ItemTouchHelperSwipeAdapter;

import java.util.List;

public class NewsListCardView extends HorizontalScrollingListCardView<NewsListCard>
        implements ItemTouchHelperSwipeAdapter.SwipeableView {
    public interface Callback {
        void onNewsItemSelected(@NonNull NewsItemCard card);
    }

    public NewsListCardView(@NonNull Context context) {
        super(context);
    }

    @Override public void setCard(@NonNull NewsListCard card) {
        super.setCard(card);
        header(card);
        set(new RecyclerAdapter(card.items()));
    }

    private void header(@NonNull NewsListCard card) {
        CardHeaderView header = new CardHeaderView(getContext())
                .setTitle(R.string.view_card_news_title)
                .setSubtitle(DateUtil.getFeedCardDateString(card.date().baseCalendar()))
                .setImage(R.drawable.icon_in_the_news)
                .setImageCircleColor(R.color.gray_disabled)
                .setCard(card)
                .setCallback(getCallback());
        header(header);
    }

    private class RecyclerAdapter extends HorizontalScrollingListCardView.RecyclerAdapter<NewsItemCard> {
        RecyclerAdapter(@NonNull List<NewsItemCard> items) {
            super(items);
        }

        @Nullable @Override protected FeedAdapter.Callback callback() {
            return getCallback();
        }

        @Override
        public void onBindViewHolder(DefaultViewHolder<HorizontalScrollingListCardItemView> holder, int i) {
            final NewsItemCard card = item(i);
            holder.getView().setText(card.text());
            holder.getView().setImage(card.image());
            holder.getView().setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getCallback() != null) {
                        getCallback().onNewsItemSelected(card);
                    }
                }
            });
        }
    }
}
