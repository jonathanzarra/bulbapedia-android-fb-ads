package org.bulbagarden.feed.model;

import android.content.Context;
import android.support.annotation.NonNull;

import org.bulbagarden.feed.ads.AdsCardView;
import org.bulbagarden.feed.announcement.AnnouncementCardView;
import org.bulbagarden.feed.becauseyouread.BecauseYouReadCardView;
import org.bulbagarden.feed.continuereading.ContinueReadingCardView;
import org.bulbagarden.feed.featured.FeaturedArticleCardView;
import org.bulbagarden.feed.image.FeaturedImageCardView;
import org.bulbagarden.feed.mainpage.MainPageCardView;
import org.bulbagarden.feed.mostread.MostReadCardView;
import org.bulbagarden.feed.news.NewsListCardView;
import org.bulbagarden.feed.offline.OfflineCardView;
import org.bulbagarden.feed.progress.ProgressCardView;
import org.bulbagarden.feed.random.RandomCardView;
import org.bulbagarden.feed.searchbar.SearchCardView;
import org.bulbagarden.feed.view.FeedCardView;
import org.bulbagarden.model.EnumCode;
import org.bulbagarden.model.EnumCodeMap;

public enum CardType implements EnumCode {
    SEARCH_BAR(0) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new SearchCardView(ctx);
        }
    },
    CONTINUE_READING(1) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new ContinueReadingCardView(ctx);
        }
    },
    BECAUSE_YOU_READ_LIST(2) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new BecauseYouReadCardView(ctx);
        }
    },
    MOST_READ_LIST(3) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new MostReadCardView(ctx);
        }
    },
    FEATURED_ARTICLE(4) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new FeaturedArticleCardView(ctx);
        }
    },
    RANDOM(5) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new RandomCardView(ctx);
        }
    },
    MAIN_PAGE(6) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new MainPageCardView(ctx);
        }
    },
    NEWS_LIST(7) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new NewsListCardView(ctx);
        }
    },
    FEATURED_IMAGE(8) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new FeaturedImageCardView(ctx);
        }
    },
    BECAUSE_YOU_READ_ITEM(9),
    MOST_READ_ITEM(10),
    NEWS_ITEM(11),
    NEWS_ITEM_LINK(12),
    ANNOUNCEMENT(13) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    SURVEY(14) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    FUNDRAISING(15) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    OFFLINE(98) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new OfflineCardView(ctx);
        }
    },
    PROGRESS(99) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new ProgressCardView(ctx);
        }
    },
    ADS(100) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AdsCardView(ctx);
        }
    };

    private static final EnumCodeMap<CardType> MAP = new EnumCodeMap<>(CardType.class);
    private final int code;

    @NonNull public static CardType of(int code) {
        return MAP.get(code);
    }

    @NonNull public FeedCardView<?> newView(@NonNull Context ctx) {
        throw new UnsupportedOperationException();
    }

    @Override public int code() {
        return code;
    }

    CardType(int code) {
        this.code = code;
    }
}
