package org.bulbagarden.feed.progress;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class ProgressCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.PROGRESS;
    }
}
