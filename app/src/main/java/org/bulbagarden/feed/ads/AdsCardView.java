package org.bulbagarden.feed.ads;

import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.bulbagarden.R;
import org.bulbagarden.feed.view.DefaultFeedCardView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdsCardView extends DefaultFeedCardView<AdsCard> {

    @BindView(R.id.adView) AdView adView;
    public AdsCardView(Context context) {
        super(context);
        inflate(getContext(), R.layout.view_feed_ad, this);
        ButterKnife.bind(this);
        loadAd();
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}
