package org.bulbagarden.feed.ads;

import org.bulbagarden.dataclient.WikiSite;
import org.bulbagarden.feed.dataclient.DummyClient;
import org.bulbagarden.feed.model.Card;

public class AdsClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new AdsCard();
    }
}
