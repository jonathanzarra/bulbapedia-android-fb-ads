package org.bulbagarden.feed.ads;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class AdsCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.ADS;
    }
}
