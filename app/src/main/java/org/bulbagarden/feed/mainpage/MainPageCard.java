package org.bulbagarden.feed.mainpage;

import android.support.annotation.NonNull;

import org.bulbagarden.feed.model.Card;
import org.bulbagarden.feed.model.CardType;

public class MainPageCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.MAIN_PAGE;
    }
}
