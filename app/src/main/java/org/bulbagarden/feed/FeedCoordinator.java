package org.bulbagarden.feed;

import android.content.Context;
import android.support.annotation.NonNull;

import org.bulbagarden.billing.BillingManager;
import org.bulbagarden.feed.ads.AdsClient;
import org.bulbagarden.feed.aggregated.AggregatedFeedContentClient;
import org.bulbagarden.feed.announcement.AnnouncementClient;
import org.bulbagarden.feed.becauseyouread.BecauseYouReadClient;
import org.bulbagarden.feed.continuereading.ContinueReadingClient;
import org.bulbagarden.feed.mainpage.MainPageClient;
import org.bulbagarden.feed.random.RandomClient;
import org.bulbagarden.feed.searchbar.SearchClient;

class FeedCoordinator extends FeedCoordinatorBase {

    FeedCoordinator(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void buildScript(int age) {
        if (age == 0) {
            addPendingClient(new SearchClient());
            addPendingClient(new AnnouncementClient());
        }
        addPendingClient(new AggregatedFeedContentClient());
        addPendingClient(new ContinueReadingClient());
        if (age == 0) {
            addPendingClient(new MainPageClient());
        }
        addPendingClient(new BecauseYouReadClient());

        if (age == 0) {
            addPendingClient(new RandomClient());

            if (!BillingManager.getInstance().isPro()) {
                addPendingClient(new AdsClient());
            }
        }
    }
}
