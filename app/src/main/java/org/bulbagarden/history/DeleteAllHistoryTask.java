package org.bulbagarden.history;

import android.content.Context;

import org.bulbagarden.WikipediaApp;
import org.bulbagarden.concurrency.SaneAsyncTask;

/** AsyncTask to clear out article history entries. */
public class DeleteAllHistoryTask extends SaneAsyncTask<Void> {
    private final WikipediaApp app;

    public DeleteAllHistoryTask(Context context) {
        app = (WikipediaApp) context.getApplicationContext();
    }

    @Override
    public Void performTask() throws Throwable {
        app.getDatabaseClient(HistoryEntry.class).deleteAll();
        return null;
    }
}
