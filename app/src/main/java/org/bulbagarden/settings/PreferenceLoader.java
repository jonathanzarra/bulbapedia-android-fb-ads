package org.bulbagarden.settings;

/*package*/ interface PreferenceLoader {
    void loadPreferences();
}
