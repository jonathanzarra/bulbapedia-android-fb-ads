package org.bulbagarden;

public interface BackPressedHandler {
    boolean onBackPressed();
}
