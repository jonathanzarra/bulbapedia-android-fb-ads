package org.bulbagarden.test.view;

import android.support.annotation.DrawableRes;

public interface TestImg {
    @DrawableRes int id();
    boolean isNull();
}
